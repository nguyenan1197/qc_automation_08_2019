<?php
namespace Page\Acceptance;

class FirstPage
{
    // include url of current page
    public static $URL = '/administrator';

    /**
     * Khai báo xpath trường username
     */
    public static $userNameField = '//input[@name="username"]';

    /**
     * Khai báo xpath trường password
     */
    public static $passwordField = '//input[@name="passwd"]';

    /**
     * Khai báo xpath nút Login
     */
    public static $loginButton = '//button[@tabindex="5"]';

    /**
     * @var string
     */
    public static $controlText = 'Control Panel';

    /**
     * @var \AcceptanceTester;
     */
    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }
}
