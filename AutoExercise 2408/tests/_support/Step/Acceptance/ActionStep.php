<?php
namespace Step\Acceptance;

use AcceptanceTester;
use Exception;
use Page\Acceptance\ActionPage;
use WebDriverKeys;

class ActionStep extends AcceptanceTester
{
    /**
     * @param $username
     * @param $password
     * Test Đăng nhập
     */
    public function Login($username, $password)
    {
        $I = $this;
        $I->wantToTest('Login Success!');
        $I->amOnPage(ActionPage::$URl);
        $I->fillField(ActionPage::$usernameField, $username);
        $I->pressKey(ActionPage::$usernameField, WebDriverKeys::TAB);
        $I->fillField(ActionPage::$passwordField, $password);
        $I->pressKey(ActionPage::$passwordField, WebDriverKeys::ENTER);
        $I->seeInCurrentUrl(ActionPage::$homePageURL);
    }

    /**
     * @throws Exception
     * Test thêm mới categories
     */
    public function addAction()
    {
        $I = $this;
        $I->wantToTest('Add new');
        $I->click(ActionPage::$contentDropdown);
        $I->click(ActionPage::$categoriesButton);
        $I->waitForText('Categories',5);
        $I->click(ActionPage::$newButton);
        $I->waitForText('New Category',5);
        $I->click(ActionPage::$titleField);
        $I->fillField(ActionPage::$titleField,'An 7');
        $I->click(ActionPage::$saveButton);
        $I->wait(10);
    }

    /**
     * @throws Exception
     * Test edit An 4
     */
    public function editAction()
    {
        $I = $this;
        $I->wantToTest('Edit');
        $I->click(ActionPage::$contentDropdown);
        $I->click(ActionPage::$categoriesButton);
        $I->click(ActionPage::$editButton);
        $I->waitForText('Edit Category',2);
        $I->wait(3);
        $I->click(ActionPage::$textArea);
        $I->fillField(ActionPage::$textArea,'Hello World');
        $I->wait(2);
        $I->click(ActionPage::$saveButton);
        $I->wait(3);
    }

    /**
     * Test xóa categories An 4
     */
    public function deleteAction()
    {
        $I = $this;
        $I->wantToTest('Delete');
        $I->click(ActionPage::$contentDropdown);
        $I->click(ActionPage::$categoriesButton);
        $I->waitForElement(ActionPage::$checkboxDelete, 40);
        $I->scrollTo(ActionPage::$checkboxDelete);
        $I->waitForElementVisible(ActionPage::$checkboxDelete, 40);
        $I->click(ActionPage::$checkboxDelete);
        $I->click(ActionPage::$trashButton);
        $I->click(ActionPage::$searchButton);
        $I->click(ActionPage::$selectecStatusDropdown);
        $I->waitForElementVisible(ActionPage::$trashedOption,5);
        $I->click(ActionPage::$trashedOption);
        $I->click(ActionPage::$searchField);
        $I->fillField('An 4');
        $I->pressKey(ActionPage::$searchField,WebDriverKeys::ENTER);
        $I->wait(5);

        $I->wait(5);
    }
}