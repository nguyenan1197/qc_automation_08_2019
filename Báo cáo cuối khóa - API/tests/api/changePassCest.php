<?php 

use Step\Api\changePassStep;
class changePassCest
{
    protected $user;
    protected $pwd;
    protected $oldPwd;
    protected $newPwd;


    public function __construct()
    {
        $this->user = 'an.student@gmail.com';
        $this->pwd = '@Noname1197';
        $this->oldPwd = '@Noname1197';
        $this->newPwd = 'Abc@123456';
    }

    /**
     * @param changePassStep $I
     * @throws Exception
     */
    public function changePass(changePassStep $I)
    {
        $I->wantTo('ĐĂNG NHẬP VÀ ĐỔI MẬT KHẨU');
        $token = $I->Login($this->user, $this->pwd);
        $I->changePass($token, $this->oldPwd, $this->newPwd);
    }

    /**
     * @param changePassStep $I
     * @throws Exception
     */
    public function loginAgain(changePassStep $I)
    {
        $I->wantTo('KIỂM TRA ĐĂNG NHẬP VỚI MẬT KHẨU CŨ');
        $I->LoginFailed($this->user, $this->pwd);
    }


}
