<?php 
use Step\Acceptance\ActionStep;
class ActionCest
{
    protected $username; //Khai báo biến username
    protected $password; //Khai báo biến password

    public function __construct()
    {
        $this->username = 'admin'; //Gán giá trị cho biến username
        $this->password = 'admin'; //Gán giá trị cho biến password
    }

    /**
     * @param ActionStep $I
     * Test Đăng nhập thành công
     * @throws Exception
     */
    public function testFunction(ActionStep $I)
    {
        $I->Login($this->username, $this->password);
        //$I->addAction();
        $I->deleteAction();
        //$I->editAction();
    }

}
