<?php


namespace Step\Api;


use Codeception\Util\HttpCode;
use Page\Api\getInfoCoursePage;

class getInforCourseStep extends \ApiTester
{
    /**
     * @param $user
     * @param $pwd
     * @return mixed
     * @throws \Exception
     * student login
     */
    public function Login($user, $pwd)
    {
        $I = $this;
        $I->comment('ĐĂNG NHẬP TÀI KHOẢN STUDENT');
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST(getInfoCoursePage::$URL, [
            getInfoCoursePage::$userName => $user,
            getInfoCoursePage::$passWord => $pwd
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $result = $I->grabDataFromResponseByJsonPath('$.result');
        $token = $result[0]['token'];
        echo "Token: ";
        $I->comment($token);
        $I->comment('ĐĂNG NHẬP THÀNH CÔNG.');
        return $token;
    }

    /**
     * @param $token
     * @throws \Exception
     */
    public function getInfoCourse($token)
    {
        $I = $this;
        echo "\n";
        $I->comment('LÂY THÔNG TIN KHÓA HỌC');
        $I->haveHttpHeader('Authorization', 'x-access-token '.$token);
        $I->sendGET(getInfoCoursePage::$urlGetCourse);
        $I->seeResponseCodeIs(HttpCode::OK);

        $result = $I->grabDataFromResponseByJsonPath('$.result.record');
        $desc = $result[0][0]['desc'];
        $I->assertEquals('Khóa học Automation của An',$desc);
        $I->comment('LẤY THÔNG TIN KHÓA HỌC THÀNH CÔNG.');
    }

    /*public function getSchedulesByCourse()
    {
        $I = $this;
        $I->comment('LẤY THÔNG TIN LỊCH HỌC CỦA KHÓA HỌC');
        $I->sendGET(getInfoCoursePage::$urlGetSchedulebyCourse, ['courseId' => '5d9856db9cf1f90d5191e326']);
        $I->seeResponseCodeIs(HttpCode::OK);
    }*/

    /**
     * @param $token
     * @throws \Exception
     */
    public function getDocumentByUser($token)
    {
        $I = $this;
        echo "\n";
        $I->comment('LẤY DANH SÁCH THƯ MUC TÀI LIỆU');
        $I->haveHttpHeader('Authorization','x-access-token '.$token);
        $I->sendGET(getInfoCoursePage::$urlGetDocbyUser);
        $I->seeResponseCodeIs(HttpCode::OK);

        $result = $I->grabDataFromResponseByJsonPath('$.result.record');
        $name = $result[0][0]['name'];
        $I->assertEquals("Folder's An", $name);
        $I->comment('LẤY DANH SÁCH THƯ MỤC THÀNH CÔNG.');
    }
}