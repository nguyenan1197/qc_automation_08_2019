<?php
namespace Page\Api;

class getUpChangePage
{
    // include url of current page
    public static $URL = '/user/login';
    public static $urlGetUp = '/user';
    public static $urlChangePass = '/user/password';
    public static $userName = 'username';
    public static $passWord = 'password';
    public static $oldPassword = 'oldPassword';
    public static $Password = 'password';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \ApiTester;
     */
    protected $apiTester;

    public function __construct(\ApiTester $I)
    {
        $this->apiTester = $I;
    }

}
