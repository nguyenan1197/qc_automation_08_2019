<?php
namespace Step\Api;
use Codeception\Util\HttpCode;
use Exception;
use Page\Api\getUpChangePage;

class changePassStep extends \ApiTester
{
    /**
     * @param $user
     * @param $pwd
     * @return mixed
     * @throws Exception
     */
    public function Login($user, $pwd)
    {
        $I = $this;
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST(getUpChangePage::$URL, [
            getUpChangePage::$userName => $user,
            getUpChangePage::$passWord => $pwd
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $result = $I->grabDataFromResponseByJsonPath('$.result');
        $token = $result[0]['token'];
        echo "Token: ";
        $I->comment($token);
        $I->comment('ĐĂNG NHẬP THÀNH CÔNG');
        return $token;
    }

    /**
     * @param $token
     * @param $oldPwd
     * @param $newPwd
     */
    public function changePass($token, $oldPwd, $newPwd)
    {
        $I=$this;
        $I->haveHttpHeader('Authorization','x-access-token '.$token);
        $I->sendPUT(getUpChangePage::$urlChangePass,[
            getUpChangePage::$oldPassword => $oldPwd,
            getUpChangePage::$Password => $newPwd
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->comment('THAY ĐỔI PASSWORD THÀNH CÔNG');
    }

    /**
     * @param $user
     * @param $pwd
     * @throws Exception
     */
    public function LoginFailed($user, $pwd)
    {
        $I = $this;
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST(getUpChangePage::$URL, [
            getUpChangePage::$userName => $user,
            getUpChangePage::$passWord => $pwd
        ]);
        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $result = $I->grabDataFromResponseByJsonPath('$.message');
        $I->assertEquals('Sai tên người dùng hoặc mật khẩu',$result[0]);
        $I->comment('ĐĂNG NHẬP THẤT BẠI');
    }
}