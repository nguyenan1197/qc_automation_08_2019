<?php
namespace Page\Acceptance;

class ActionPage
{
    public static $URl = '/administrator';
    public static $usernameField = '//input[@name="username"]';
    public static $passwordField = '//input[@name="passwd"]';
    public static $homePageURL = '/administrator/index.php';
    public static $contentDropdown = '//a[text()="Content "]';
    public static $categoriesButton = '//a[text()="Categories"]';
    public static $newButton = '//button[@class="btn btn-small button-new btn-success"]';
    public static $titleField = '//input[@name="jform[title]"]';
    public static $saveButton = '//button[@class="btn btn-small button-save"]';
    public static $checkboxDelete = '(//input[@value="23"])[2]'; //checkbox xóa An 4
    public static $trashButton = '//button[@class="btn btn-small button-trash"]';
    public static $editButton = '(//a[@data-original-title="Edit"])[5]'; //nút edit An 4
    public static $textArea = '//div[@id="mceu_89"]';
    //public static $textArea1 = '//div[@class="span9"]';


    public static $searchButton = '//button[@class="btn hasTooltip js-stools-btn-filter"]';
    public static $selectecStatusDropdown = '//div[@id="filter_published_chzn"]';
    public static $trashedOption = '//div[@id =\'filter_published_chzn\']/div/ul/li[contains(text(),\'Trashed\')]';
    public static $searchField = '//input[@id="filter_search"]';
    public static $idAn4Trashed = '//td/span[@title="41-42"]';




    protected $acceptanceTester;

    public function __construct(\AcceptanceTester $I)
    {
        $this->acceptanceTester = $I;
    }

}
