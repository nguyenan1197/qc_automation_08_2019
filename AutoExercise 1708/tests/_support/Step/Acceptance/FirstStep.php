<?php
namespace Step\Acceptance;
use Page\Acceptance\FirstPage;
class FirstStep extends \AcceptanceTester
{
    public function testLogin($username, $password)
    {
        $I = $this; //biến mặc định sẽ là this, thay nó bằng I hoặc bằng thứ gì bạn muốn)
        $I->amOnPage(FirstPage::$URL); //Truy cập vào URL

        $I->click(FirstPage::$userNameField); //Click vào trường userName
        $I->fillField(FirstPage::$userNameField, $username); //Nhập vào biến username

        $I->click(FirstPage::$passwordField);
        $I->fillField(FirstPage::$passwordField, $password); //Nhập vào biến password

        $I->pressKey(FirstPage::$passwordField, \WebDriverKeys::ENTER);
        //$I->click(FirstPage::$loginButton);

        $I->waitForText(FirstPage::$controlText,5);
        //$I->seeInCurrentUrl("/index.php");

    }

}