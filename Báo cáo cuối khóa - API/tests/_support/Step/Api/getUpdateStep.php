<?php
namespace Step\Api;
use Page\Api\getUpChangePage;
use Codeception\Util\HttpCode;

class getUpdateStep extends \ApiTester
{
    /**
     * @param $user
     * @param $pwd
     * @return mixed
     * @throws \Exception
     * Đăng nhập vào hệ thống
     */
    public function Login($user, $pwd)
    {
        $I = $this;
        $I->haveHttpHeader('Content-Type', 'application/x-www-form-urlencoded');
        $I->sendPOST(getUpChangePage::$URL, [
            getUpChangePage::$userName => $user,
            getUpChangePage::$passWord => $pwd
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);
        $result = $I->grabDataFromResponseByJsonPath('$.result');
        $token = $result[0]['token'];
        echo "Token: ";
        $I->comment($token);
        $I->comment('ĐĂNG NHẬP THÀNH CÔNG');
        return $token;
    }

    /**
     * @param $token
     * @throws \Exception
     * Lấy thông tin User đăng nhập vào hệ thống
     */
    public function Get($token)
    {
        $I = $this;
        $I->haveHttpHeader('Authorization', 'x-access-token ' . $token);
        $I->sendGET(getUpChangePage::$urlGetUp);
        $I->seeResponseCodeIs(HttpCode::OK);
        $result = $I->grabDataFromResponseByJsonPath('$.result');
        $username = $result[0]['username'];
        $I->assertEquals('an.student@gmail.com', $username);
        $I->comment('LẤY THÔNG TIN THÀNH CÔNG');
    }

    /**
     * @throws \Exception
     * Cập nhật thông tin User đang đăng nhập hệ thống
     */
    public function Update()
    {
        $I = $this;
        $I->sendPUT(getUpChangePage::$urlGetUp,[
            'firstName' => 'Nguyễn Xuân',
            'lastName' => 'Anh',
            'gender' => 2,
        ]);
        $I->seeResponseCodeIs(HttpCode::OK);

        $result = $I->grabDataFromResponseByJsonPath('$.result');
        $firstName = $result[0]['firstName'];
        $lastName = $result[0]['lastName'];
        $gender = $result[0]['gender'];
        $I->assertEquals('Nguyễn Xuân',$firstName);
        $I->assertEquals('Anh',$lastName);
        $I->assertEquals(2,$gender);
        $I->comment('CẬP NHẬT THÔNG TIN THÀNH CÔNG');
    }
}