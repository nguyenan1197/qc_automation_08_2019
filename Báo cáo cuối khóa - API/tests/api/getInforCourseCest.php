<?php

use Step\Api\getInforCourseStep;

class getInforCourseCest
{
    protected $username;
    protected $pwd;

    /**
     * getInforCourseCest constructor.
     */
    public function __construct()
    {
        $this->username = 'an.student@gmail.com';
        $this->pwd = '@Noname1197';
    }

    /**
     * @param getInforCourseStep $I
     * @throws Exception
     */
    public function getInforCourse(getInforCourseStep $I)
    {
        $I->wantTo('TEST LẤY THÔNG TIN KHÓA HỌC');
        $token = $I->Login($this->username, $this->pwd);
        $I->getInfoCourse($token);
    }

    /*public function getSchedulesByCourse(getInforCourseStep $I)
    {
        $I->getSchedulesByCourse();
    }*/

    /**
     * @param getInforCourseStep $I
     * @throws Exception
     */
    public function getDocSchedules(getInforCourseStep $I)
    {
        $I->wantTo('TEST LẤY DANH SÁCH THƯ MỤC TÀI LIỆU');
        $token = $I->Login($this->username, $this->pwd);
        $I->getDocumentByUser($token);
    }

}